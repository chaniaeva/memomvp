package com.bootcamp.memomvp.view.update

interface UpdateView {
    fun onSuccessEdit(result : String)
}